== UML

=== Structure diagrams

Use case diagrams as well as class diagrams and object diagrams are *structure diagrams*.

These diagrams model elements statically.
They do not deal with dynamic aspects (time flow, sequence of operations, ...).

When what is to be modeled is complex, it is better to draw several complementary diagrams, each focusing on a particular aspect.
For example: the hierarchical structure of the different elements, their logical grouping, the state of the system at a given moment T, the way the elements collaborate with each other, and so on.

Often, even focusing on a particular aspect is too complex to summarize that aspect in a single diagram.


[[uml_usecases]]
=== Use cases diagrams

A *use case diagram* is used to understand and structure requirements.
It is not intended to be exhaustive (high abstraction, low level of detail),
but to clarify and organize of needs.
For example, it allows sorting the needs of end users between specific needs
and needs that are in fact covered by other needs or needs that are just constraints or conditions.
Defining the actual requirements determines the scope of the project
by identifying the main functionalities of the system being designed.
It is those objectives that must be kept in mind throughout the project.

The use case diagram classifies the different actors and the way they use (interact with) the system.

image::uml/usecase_simple.png[caption="Figure 01:", title="Simple use case", alt="Simple use case"]

==== UML elements

==== Relations

Relations are links uniting two or more elements.
The relationship can be of different kinds: use, extension, dependence, aggregation, etc.
The nature of a relationship can be detailed using a *stereotype* (`<< stereotype >>`).

As far as use case diagrams are concerned, UML recognizes three types of relationship: inclusion, extension and generalization.

An *inclusion* relationship represents the decomposition of a task into several subtasks.

image::uml/usecase_includes.png[caption="Figure 02.1:", title="Inclusion relationship", alt="Inclusion"]

An *extension* relationship represents the fact that one task can upon another.

image::uml/usecase_extends.png[caption="Figure 02.2:", title="Extension relationship", alt="Extension"]

A *generalization* relationship represents the fact that one task can be substituted by another, more specialized one.

image::uml/usecase_generalizes.png[caption="Figure 02.3:", title="Generalization relationship", alt="Generalization"]

==== Actor

An actor is an *entity external to the system*, but acting on it.
It can be a natural person, another system already in place, etc.
In response to each action of an actor, the system performs a feedback action that corresponds to the actor's need.

The same natural person can play several different roles in the same project.
This is why actors are mainly defined by their role in a UML diagram.

===== Use case

*Action* of the system in response to the need expressed by an actor.
Several use cases can be linked together (dependency, hierarchy relationships).
To facilitate understanding and reuse, use cases can be organized in *packages*.

===== Package

Organizational element that brings together other elements into a *logical* group.
The elements it contains are accessible via an access *interface*.
It also serves as a *namespace*.

image::uml/usecase_package.png[caption="Figure 04:", title="Grouping into a package", alt="Logical package"]

A package can depend on one or more other packages.



=== Classes and Objects diagrams

UML allows to model *class diagrams* as well as *object diagrams*.

[NOTE,caption=""]
====
A little reminder of OOP?

* A class is a name, attributes and methods.
* An object is an instance of a class.
====

The notations are as follows.

image::uml/class_simple.png[caption="Figure 05.1:", title="Class `Person`.", alt="Class"]
image::uml/class_abstract.png[caption="Figure 05.2:", title="Class abstract `Work`.", alt="Class abstract"]
image::uml/class_instance_anonymous.png[caption="Figure 05.3:", title="Anonymous instance of the class `Person`.", alt="Anonymous object"]
image::uml/class_instance_named.png[caption="Figure 05.4:", title="Named instance of the class `Person`.", alt="Named object"]
image::uml/class_instance.png[caption="Figure 05.5:", title="Named instance of an anonymous class.", alt="Anonymous class"]
image::uml/class_instance_attributes.png[caption="Figure 05.6:", title="Named instance of the class `Person`. The values of some attributes _for this instance_ are specified.", alt="Object with attributes"]
image::uml/class_collection.png[caption="Figure 05.7:", title="Collection of anonymous instances of the class `Person`.", alt="Collection of objects"]

==== Documentation of a class

image::uml/class_description.png[caption="Figure 05.8:", title="More complete documentation of the `Person` class.", alt="Documented class"]

* Attributes whose visibility is *public* are preceded by a `+`.
* Attributes whose visibility is *protected* are preceded by a `#`.
* Attributes whose visibility is *private* are preceded by a `-`.

[NOTE.warning,caption=""]
====
Just because the representation of a class within a given diagram
does not contain certain attributes or methods does not mean that it does not have them.

Similarly, the visibility of certain attributes may be omitted.

The representation of a UML element is an *abstraction* which contains more or less details
according to what the diagram containing it requires to be understandable.
The purpose of a given diagram is to be *clear*.
Completeness, on the other hand, is achieved by combining all the diagrams in a design document.
====



==== Associations

Two classes or objects are associated when they are connected in one way or another.

The preferred reading direction of an association can be specified next to its label. 

image::uml/association_direction_read.png[caption="Figure 06.1:", title="Reading direction", alt="Association"]

More than two elements can obviously be associated.

image::uml/association_multiple.png[caption="Figure 06.2:", title="Multiple Association", alt="Multiple Association"]

==== Role

Specifying the *role* of each element in an association is sometimes essential when the relationship goes both ways.

image::uml/association_role.png[caption="Figure 06.3:", title="Roles", alt="Roles"]

==== Cardinality

The *cardinality* is the number of objects participating in an association.

* `1` Exactly one, no more and no less.
* `*` An indefinite number
* `1...n` Between one (inclusive) and `n` (inclusive)
* `n..m` Between `n` and `m`.

image::uml/association_cardinality.png[caption="Figure 06.4:", title="Cardinalities", alt="Cardinalities"]

[NOTE.warning,caption=""]
====
Be careful, it is only useful to model what is required, not real life!
Obviously, in real life, a person can be enrolled in several universities,
but the system this diagram models doesn't seem to need that level of detail...
====

==== Constraints

The *constraints* are used to specify the scope of another element.
For example, a constraint can specify a role or restrict a number of instances.

image::uml/association_constraints_1.png[caption="Figure 06.5:", title="Constraints", alt="Constraints"]
image::uml/association_constraints_2.png[caption="Figure 06.6:", title="Constraints", alt="Constraints"]

===== One-way relationship

By default, an association is navigable in both directions.
But sometimes, it is necessary to indicate that an element does not "know" another one.

image::uml/association_directional.png[caption="Figure 06.7:", title="One-way relationship", alt="Direction"]]

===== Prohibited Relationship

Sometimes it is useful to indicate that an association between two elements is to be avoided.

image::uml/association_forbidden.png[caption="Figure 06.8:", title="Forbidden relationship", alt="Prohibited"]


===== N-ary relationship and association class

An *association class* allows to understand the relation between the elements.

image::uml/association_naire.png[caption="Figure 06.8:", title="N-ary relationship with association class", alt="Association class"]

[NOTE.warning,caption=""]
====
Such associations can be difficult to decipher.
It may be preferable to limit their use, and to model other relationships.
====

===== Inheritance

Used to model the hierarchy that links the different classes.
This modeling can be :

* *ascending* : starting from the most concrete objects,
  and *generalizing* progressively by factoring their properties
  (attributes and methods) in super classes.
* *descending*: starting from generic objects and *specializing* them
  by extending their properties into more specific classes.

image::uml/class_inheritance.png[caption="Figure 07:", title="Heritage", alt="Heritage"]

[[solid_principle]]
[NOTE.warning,caption="SOLID principle"]
====
The way classes are hierarchized is often subjective.
However, the best known basic principles are represented by the acronym *SOLID* :

* *S*: Single responsibility +
  A class must have one and only one responsibility, which it must fulfil completely.
* *O*: Open/closed +
  A class must be open to extension (_ie._ easily specializable),
  but closed to modification (its behaviour must not be able to be altered).
* *L*: Liskov substitution +
  An instance of type T must be replaceable by an instance of type G,
  such as G is subtype of T, without changing the consistency of the program.
* *I*: Interface segregation +
  It is better to create several interfaces, each tailored to a specific customer, rather than a single general interface.
* *D*: Dependency inversion +
  Abstractions should not depend on implementation details.

The "God object" is a counter-example (anti-pattern) to these principles.
====

===== Aggregation

Whereas inheritance is a "Is a" realationship,
Aggregation is a "Has a" relationship.
It is a non-symmetrical relationship.
It expresses a relation of *subordination*.

An aggregation can mean that one element _is part of another (the other is an "aggregate" of the one).
It can also mean that a change of state or an action on one has consequences on the other.

One element can be aggregated into several others.

The life cycle of an aggregate may be different from that of the elements it aggregates.
In other words, an instance of an aggregated element can exist without an aggregate, and vice versa.

image::uml/association_agregation.png[caption="Figure 08.1:", title="Aggregation", alt="Aggregation"]

====== Composition

Composition is a strong aggregation.

A component can only be bound to one compound.

The life cycles of the aggregated elements (the *components*) and the aggregate (the *composite*) are linked.
In other words, if the aggregate is destroyed, so are its components (however, the reverse is not necessarily true).

image::uml/association_composition.png[caption="Figure 08.2:", title="Composition", alt="Composition"]






=== Messages syntax

Each message can be assorted of attributes.
These attributes can be used, among other things, to indicate, for a given message:

* its position in an order
* the condition under which it is sent
* his parameters
* its recurrence

`[pre"/"] [["["guard"]"] [number] ["*"["||"]["["times"]"]] ":"] [var":="] label"("[params]")"`

* `label` message wording
* `parameters` message parameters, if any
* `var` allows you to assign the return message to a "variable".
  This allows you to use this return value as a parameter (`params') in another message, for example.
* `pre` A comma-separated list of `numbers'.
  The message is not sent until all previous messages have been sent.
  This attribute allows *synchronization* of different messages.
* `guard` Boolean expression.
  This is the condition for sending the message, called a *guard* in UML.
  The message is sent only if this condition is verified.
  The condition itself can be expressed in natural language, by a mathematical expression, and so on.
* `number` The *order* number of the message in the message sequence modeled by the diagram.
  This number can be a simple integer, or follow a more complex indexing.
* `times` allows you to send a message as many times as you want.
  By default, the sending is sequential.
  Combined with `||`, sending is parallel.
  The number `times` can be replaced with `\*` to show an indefinite number of times to be sent.
  The `*|||` number can be changed to `\**' to show an indefinite number of parallel uploads.

==== Examples

```
1: hi()
2: lmao()
3: kthxbye()
```
A sequence of three messages numbered `1`, `2`, `3`.

```
1: mood := how_are_you()
[mood = "good"] 2.a: happy_for_you()
[mood = "so-so"] 2.b: sympathize()
```
The result of the first message is used to condition the sending of the next message.
Note that the order numbers are not necessarily integers, nor do they all have to have exactly the same format.

```
4,5.1 / [3]||[i := 1..3]: message()
```
The message `message` is sent 3 times, in parallel.
These parallel sendings only occur after the `4` and `5.1` messages have been sent themselves.

```
[disk full] 4.2.a * : deleteTmpFile()
[disk full] 4.2.b : reduceSwap(20%)
```
These messages are sent simultaneously if the `disk full` condition is true.
`reduceSwap` is sent once; `deleteTmpFile` can be sent multiple times.

=== Message Types

* A simple message image:uml/message_simple.png[msg_simple,title="Simple message arrow"]
  does not specify any particular characteristics that is useful for understanding the diagram.
  Be careful, that's not why he doesn't have any!

* A synchronous message image:uml/message_sync.png[msg_sync,title="Synchronous message arrow"]
  blocks the sender until the recipient's reply is received.

* A message with timeout image:uml/message_timeout.png[msg_timeout,title="Message with timeout arrow"]
  blocks the sender for a while.
  The precise blocking time can be specified by a constraint.
  The sender is locked, either :

** until the receiver's reply is received
** until the end of the timeout

* An asynchronous message image:uml/message_async.png[msg_async,title="Asynchronous message arrow"]
  does not interrupt the flow of execution of the transmitter.
  The sender does not have a priori any confirmation that the message has been taken into account by the recipient.
  This is why the return of an asynchronous message should always be explicitly modeled.

* An expected message image:uml/message_expected.png[msg_expected,title="Expected message arrow"] is a message that triggers an action on the part of its recipient only if the latter has previously put himself on hold for this message.

An element can send a message to itself.
This represents an internal activity within the element, such as a payroll period, for example.



=== Collaboration diagram

This diagram allows to model the interactions between objects in a given context.
The context of the interactions is given by the state of the interacting objects.

This diagram is also called a communication diagram.

image::uml/collaboration.png[caption="Figure 09:", title="Example of a collaboration diagram", alt="Collaboration"]

=== Sequence diagram

This diagram allows to model the interactions between objects over time.
It focuses on the chronology of message sending.

On a sequence diagram, time flows vertically.
The events that occur first are at the top of the diagram,
and later ones can be viewed in their chronological order,
as the user's eye moves down.

The horizontal order of the elements (actors, objects, ...) is not important,
excepted for readability purposes.

A sequence diagram can illustrate a use case dynamically.

image::uml/sequence.png[caption="Figure 10:", title="Sequence diagram", alt="Client/server dialog"]

==== Period of activity

A sequence diagram can be used to represent a period of activity of an object
by means of a rectangular area representing the period of the object's life
during which this object is active (in the sense of the diagram considered).

This rectangular area can also be used to represent a recursion.

==== Conditions and iterations

To represent a conditional execution on a sequence diagram,
it is possible to duplicate the lifeline of the object concerned.
However, this is not necessary: one can express the conditions
in the description of a message.

It is also possible to represent an iteration.

==== Destruction of an object

It is possible to represent the end of the life of an object
using a cross at the bottom of his lifeline.



=== Activity diagram

An activity diagram is used to link different activities together.
Moving from one activity to another is done via a transition.
A transition linking two activities is used automatically as soon as the first activity ends.

image::uml/activity_transition.png[caption="Figure 12.1:", title="Transition of an activity diagram", alt="Arrow"]

The beginning of an activity diagram is marked by a solid black circle.
It is indispensable.

The end of an activity diagram is marked by a black circle surrounded by a circle.
It is optional, because an activity diagram is finished when there is no more activity in progress.

To organize and make it easier to read an activity diagram,
it is possible to materialize "activity corridors".

image::uml/activity_corridors.png[caption="Figure 12.2:", title="Activity Corridors", alt="Corridors"]

A conditional transition is represented using one or more diamonds and guards.

image::uml/activity.png[caption="Figure 12.3:", title="Conditional transitions", alt="Choosing a pet"]

Several activities can be parallelized using synchronization bars.
Synchronization bars respect the following rules:

* a bar is passed through only when all the activities that reach it have ended
* activities starting from a bar all start at the same time

image::uml/activity_parallelization.png[caption="Figure 12.4:", title="Parallelization", alt="Starting a car"]

