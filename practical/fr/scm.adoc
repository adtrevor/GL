﻿:source-highlighter: prettify
:source-highlighter: highlightjs
:icons: font
:linkattrs:

= Gestion de versions
Régis WITZ <rwitz@unistra.fr>
:doctype: book
:toc:
:toc-title:
:toclevels: 1

== Pré-requis

Ces exercices nécessitent d'avoir link:https://git-scm.com/book/en/v2/Getting-Started-Installing-Git[installé git] sur votre machine.
Il vous faudra aussi au moins un éditeur de texte, ou mieux, un link:https://korben.info/editeurs-de-code-et-de-texte-liste-ultime.html[éditeur de code].

Assurez-vous de satisfaire ces pré-requis avant de commencer ces exercices.

== Introduction

La link:https://fr.wikipedia.org/wiki/Logiciel_de_gestion_de_versions[*gestion de versions*,title=Page Wikipedia sur la gestion de versions] (aussi appelée gestion de code source, ou _VCS_), comme son nom l'indique, permet de créer des *versions* d'un code source, ce qui a trois objectifs :

* Gérer une *historique* des modifications apportées à une base de code.
  Conserver une trace constante de qui a fait quoi, quand et pourquoi dans le code est important pour la *maintenabilité* de ce code.
* Permettre de *revenir en arrière*.
  Personne n'est parfait ; parfois, en dépit des précautions prises, des modifications indésirables trouvent leur chemin dans le code.
  De même, il arrive que la compréhension de comment une fonctionnalité doit être implémentée ne soit comprise qu'au cours de son implémentation.
* Et évidemment, le plus important : permettre le *travail collaboratif*.
  Hormis en cas de link:https://fr.wikipedia.org/wiki/Programmation_en_bin%C3%B4me[_pair programming_,title=Page wikipedia sur la programmation en binôme], il est rare que plusieurs développeurs puissent modifier en même temps les mêmes sections du code source sans « se marcher sur les pieds ». Un gestionnaire de source permet de travailler ensemble de manière asynchrone, en fusionnant les modifications de chacun lors d'une opération appelée *merge*.

Ces trois objectifs sont pleinement atteints si chaque utilisateur publie le plus souvent possible, en mettant dans chaque publication, ou *commit*, les modifications liées à une et une seule fonctionnalité.
Chacune de ces modifications devrait être la plus atomique, compréhensible et correcte possible, tout en restant pertinente.
Cette discipline de travail peut se résumer par la maxime anglaise « _commit early, commit often_ ».

Découvrons donc link:https://fr.wikipedia.org/wiki/Git[git,title=Page Wikipedia de git], un tel logiciel de gestion de versions.
Les bénéfices spécifiques de git sont les suivants:

* Le fait qu'il s'agisse d'un logiciel *distribué* apporte flexibilité et sécurité.
  En effet, en cas de perte du serveur central que mettent en place la plupart des organisations, il est facile de repartir du dépôt le plus récent existant au sein de l'équipe de développement.
  De plus, il est aisé pour certains développeurs de fonctionner « en marge » du dépôt central.
* Dans la plupart des cas, git est *rapide*.
  Des opérations pouvant prendre plusieurs minutes à plusieurs heures avec d'autres _VCS_ prennent souvent link:https://gist.github.com/emanuelez/1758346[moins d'une seconde,title=Analyse quantitative des performances de git].
* git offre un espace de travail versionné à chaque développeur.
  Cela permet à chacun de gérer sa propre historique avant de la partager avec autrui.
  git est donc parfaitement *adapté au travail en solo*.
* git est bien connu et link:https://stackshare.io/git[utilisé au sein de nombreuses organisations,title=Liste des utilisateurs de git].
  Ce taux d'adoption a donné lieu à de *nombreux outils tiers* venant enrichir l'expérience ou faciliter le travail de ses utilisateurs.
* git est actuellement ce qui s'approche le plus d'un *standard* en ce qui concerne la gestion de version, puisqu'il est au cœur de services comme gitlab, github, et bien d'autres.


[NOTE]
.Les autres gestionnaires de version
=====
git n'est au final qu'un gestionnaire de version parmi d'autres.
Même si c'est le plus utilisé aujourd'hui dans de nombreux contextes, cela ne signifie pas qu'il est utilisé _partout_.
Or, pour un base de code importante, passer d'un _VCS_ à l'autre n'est pas trivial, et en perdre l'historique est souvent inacceptable.
En conséquence, vous pourrez être amené à utiliser d'autres solutions.
Pour votre carrière, je vous conseille donc de tester un minimum les alternatives, ou du moins de connaître les spécificités.

On peut notamment citer :

* link:https://www.mercurial-scm.org/[*Mercurial* (hg),title=Site de Mercurial], très similaire à git.
  Si vous maîtrisez git, mercurial ne devrait pas vous poser de problème.
* link:https://subversion.apache.org/quick-start[*Subversion* (svn),title=Site de Subversion] a été le « standard » pendant très longtemps.
  Il est possible que vous le rencontriez encore dans la nature.
* link:http://cvs.nongnu.org/[*CVS* (Concurrent Version System),title=Site de CVS] est un « standard » encore plus ancien.
  Il est aujourd'hui totalement dépassé, mais ... on ne sait jamais !
* link:https://www.perforce.com/[*Perforce*,title=Site de Perforce] est une solution propriétaire qui a ses mérites.
  Elle a la préférence de link:https://www.perforce.com/customers[plusieurs entreprises,title=Liste des utilisateurs de Perforce, sur le site de Perforce], notamment dans le milieu du développement de jeux vidéos.

La plupart des concepts qui s'appliquent à git s'appliquent, bien que différemment, à ses alternatives.
Si vous désirez vous former à une alternative, vous pourrez transposer les connaissances aquises ici, de manière plus ou moins immédiate.
De manière générale, je vous recommande de comparer les différentes solutions à un problème donné chaque fois que possible, afin de pouvoir faire un choix éclairé et adapté à _vos_ propres besoins.
=====

[WARNING]
.Gestion de version ≠ Gestion de configuration
=====
À noter qu'il ne faut pas confondre gestion de version et gestion de configuration (ou _SCM_).
La gestion de configuration couvre tous les processus et outils permettant de compiler, _packager_ et déployer des logiciels.
Cela recouvre divers logiciels, et en particulier un ou plusieurs gestionnaires de version.
Cependant, le périmètre de la gestion de configuration est bien plus vaste.

Nous en reparlerons ultérieurement.
=====



== Les branches

Même avec les possibilité de travail en équipe asynchrone apporté par un gestionnaire de version, il est rare (et déconseillé) qu'une fonctionnalité entière puisse être publiée en une seule fois.
Il est donc nécessaire, afin de ne pas gêner les autres membres de l'équipe de développement dans leur propre implémentation de fonctionnalités, de toujours travailler sur une copie du code source.
Dans le contexte de la gestion de version, cette « copie » du code source s'appelle une *branche*.
La branche par défaut du code source est appelée le *master* (ou parfois le *tronc*, ou _trunk_).

Une branche pouvant être créée à partir de n'importe quelle autre branche, le risque est de voir se multiplier les conflits de _merge_, voire de perdre des fonctionnalités entières, si le cycle de vie des différentes branches n'est pas géré de manière appropriée.
Il est donc indispensable que toute équipe de développement se mette d'accord sur une *stratégie* pour gérer ses branches.
Deux stratégies sont usuelles :

* celle dite du « tronc stable, branches de développement », décrite en détail dans link:http://nvie.com/posts/a-successful-git-branching-model[_A successful git branching model_,title=Un articule expliquant la motivation derrière git flow],
* celle dite du « tronc instable, branches de livraison », décrite dans link:https://barro.github.io/2016/02/a-succesful-git-branching-model-considered-harmful[_A successful git branching model considered harmful_,title=Une opinion contraire à l'article « A successful git branching model»].

[NOTE]
.« ... _considered harmful_ » ?
=====
Aucune de ces deux stratégies n'est intrinsèquement meilleure que l'autre autre.
Même si les opinions de certains professionnels peuvent parfois être très tranchées et vous prétendre le contraire, ce qui compte avant tout c'est _d'avoir une stratégie commune_.
Plus précisément, il faut que cette stratégie rencontre l'adhésion des membres de l'équipe de développement, et qu'elle leur permettre de faire leur travail.

Tout le reste est affaire d'habitudes et de préférences personnelles.
=====

Dans la suite, nous allons aborder essentiellement la première de ces deux stratégies, car elle est supportée par un _wrapper_ de git très pratique : link:https://blog.xebia.fr/2018/03/28/gitflow-est-il-le-workflow-dont-jai-besoin/[*git flow*,title=Discussion sur git flow].
Cependant, même avec git flow, la stratégie réellement appliquée dépendra avant tout de la discipline de l'équipe de développement.
De plus, il restera possible (voire indispensable dans certains cas) de créer des branches de livraison, comme nous le verrons.

=== git flow

L'utilisation de git flow passe par une standardisation des branches.
Deux d'entre elles sont permanentes :

* `master` représente la version du logiciel actuellement en production (version courante).
* `develop` centralise toutes les fonctionnalités à livrer dans la prochaine version.

Trois autres branches, ou plutôt types de branches, ont une durée de vie temporaire :

* `feature/` regroupe tous les _commits_ liés au développement d'une fonctionnalité particulière.
  Lorsque cette fonctionnalité est terminée, et qu'elle doit être intégrée dans la prochaine version,
  une telle branche est créée à partir de `develop`, et fusionnée dans `develop`.
* `release/` est utilisée pour préparer la prochaine version.
  Elle peut être utilisée pour effectuer des tests (en particulier des tests de validation ou de recette), ou corriger les derniers bugs.
  Elle est créée à partir de `develop`, et fusionnée dans `master` et `develop`.
* `hotfix/` sert à corriger des bugs constatés en production.
  Elle est créée à partir de `master`, et fusionnée dans `master` et `develop`.



== En pratique

La suite est réalisable individuellement aussi bien qu'à plusieurs.

* en mode solo, les différents chemins utilisés seront simplement locaux.
* en mode deux joueurs (ou davantage),
  il faudra utiliser les adresses réseaux de vos homologues
  (en utilisant des url du type `user@machine:/path/vers/le/repo`),
  ou bien vous relayer sur la même machine,
  et vous coordonner avec eux quand aux opérations à réaliser. +
  Par exemple, voici la suite d'actions à réaliser à deux.

** le premier effectue les étapes 1 à 7, 10 à 12 et 15 ;
** le second effectue les étapes 8 à 14.
** Ensuite, on inverse les rôles !


=== Exercice 1: commandes de base

. Créez un dépôt vide dans un nouveau répertoire.
  Observez le répertoire `.git` ainsi créé, et essayez de comprendre son contenu.

. Renseignez vos informations personnelles pour votre dépôt.

. Créez un fichier source dans votre dépôt.
  Peu importe le langage, il faut simplement qu'il compile. +
  Par exemple, le fichier Python suivant fait très bien l'affaire:
+
[source,python]
.src/main.py
----
#!/usr/bin/python

if __name__ == '__main__':
	print "Hello, world"
----
Vous pouvez aussi faire ce TP avec un fichier C++:
+
[source,c]
.src/main.cpp
----
#include <iostream>

int main(int argc, char **argv) {
	::std::cout << argv[0] << ::std::endl;
}
----


. Compilez votre fichier source. +
  Par exemple, pour compiler le fichier `src/main.py` précédent:
+
....
python [-O] -m py_compile src/main.py
....
L'équivalent pour compiler `src/main.cpp` est simplement:
+
....
g++ [-O3] src/main.cpp
....

. Observez l'état de votre dépôt.
  Quels sont les fichiers qu'il est désirable de garder la trace ?
  Lesquels ne faut-il pas faire conserver par le gestionnaire de sources ?
  Pourquoi ?

. Créez un fichier `.gitignore` à la racine de votre dépôt pour que git ignore certains fichiers.
  Vérifiez à chaque fois l'effet de votre `.gitignore` sur l'état de votre dépôt.

. Ajoutez votre fichier source ainsi que le fichier `.gitignore` à l'index de votre dépôt.
  Observez l'état de votre dépôt. +
  Puis, commitez ces deux fichiers.
  Observez à nouveau l'état de votre dépôt. +
  Enfin, observez l'historique de votre dépôt.

. Dupliquez votre premier dépôt en un second dépôt.
  Attention, le répertoire abritant ce second dépôt doit évidement
  se trouver _à l'extérieur_ du répertoire abritant le premier dépôt.
  Le mieux est de mettre les deux dépôts côte à côte. +
  Pour faciliter votre compréhension de la suite, renseignez pour ce second dépôt
  des informations personnelles différentes de celles de votre premier dépôt.

. Observez l'état du second dépôt.
  Remarquez l'absence de fichiers non commités. +
  Vérifiez le chemin vers lequel pointe ce second dépôt.

. Rajoutez une ligne au fichier source de votre premier dépôt.
  Commitez ces modifications. +
  Puis, récupérez les modifications de votre premier dépôt dans votre second dépôt.
  Observez l'historique de votre second dépôt.

. Supprimez la ligne que vous avez rajouté précedemment du fichier source de votre premier dépôt.
  Commitez ces modifications. +
  Puis, récupérez les modifications de votre premier dépôt dans votre second dépôt.
  Observez l'historique de votre second dépôt.

. Modifiez au moins une ligne du fichier source de votre premier dépôt.
  Puis, visualisez la différence avec la dernière version commitée.
  Enfin, commitez vos modifications. +
  Modifiez _la même ligne du même fichier_ dans votre second dépôt.
  Puis, visualisez la différence avec la dernière version commitée.
  Enfin, commitez vos modifications ; attention, pour que vous compreniez mieux la prochaine étape,
  il est préférable que le commentaire de ce commit dans votre second dépôt
  soit différent du commentaire du commit que vous venez de faire dans votre premier dépôt. +
  Par exemple, vous pouvez modifier le fichier de votre premier dépôt de cette manière:
+
[source,python]
.depot1/main.py
----
	print "Hello, world !!!"
----
Et le fichier de votre second dépôt de cette manière:
+
[source,python]
.depot2/main.py
----
	print "Hello, world ???"
----

. Essayez de récupérer les modifications de votre premier dépôt dans votre second dépôt: il y a conflit.
  Observez l'état de votre second dépôt pour confirmer le conflit.

. Ouvrez le fichier en conflit de votre second dépôt.
  Repérez les marqueurs de début (`<<<<<<<`) et de fin (`>>>>>>> VERSION`) de conflit. +
  Décidez de quelle manière résoudre ce conflit.
  Vous pouvez garder le code de l'un ou l'autre de vos dépôts, ou même les deux.
  Cependant, une fois votre choix fait, il est nécessaire de supprimer les marqueurs de conflit
  `<<<<<<<`, `=======` et `>>>>>>>`. +
  Enfin, ajoutez le fichier dûment modifié à l'index et commitez-le.

. Récupérez les modifications de votre second dépôt dans votre premier dépôt.
  Comparez l'historique de votre premier dépôt avec celle de votre second dépôt.



=== Exercice 2: cherry-pick

Il arrive qu'au moment d'indexer des modifications,
le développeur s'aperçoive qu'un fichier renferme des modifications à indexer,
mais aussi une ou plusieurs modifications qui ne doivent pas être indexées. +
git permet de résoudre ce problème en indexant que certaines modifications et pas d'autres.

* Modifier plusieurs lignes de votre fichier source.
  Vous y verrez plus clair si les lignes modifiées ne sont pas contiguës
  (par exemple, ajoutez une ligne au début de votre fichier et une autre à la fin).

* Ajoutez ce fichier à l'index en utilisant l'option `--patch`.
  Observez de quelle manière cette option vous permet de n'ajouter qu'une partie de vos modifications.
  Essayez en particulier le choix `e`.



=== Exercice 3: remisage

Un développeur doit parfois interrompre son travail alors que celui-ci n'est pas prêt à être validé, et qu'il ne peut pas non plus être perdu, afin de (par exemple) corriger un bug en urgence. +
Comme nous le verrons dans l'exercice suivant, créer une branche est une solution à ce problème, mais git en propose une seconde.

Étudiez les différentes utilisations de la commande `git stash` [link:https://git-scm.com/book/fr/v2/Utilitaires-Git-Remisage-et-nettoyage[documentation,title=Documentation sur le remisage]].
Apprenez à la manier, afin d'être en mesure, à n'importe quel moment, d'interrompre vos développements pour travailler temporairment sur autr chose, commiter, et récupérer le travail précédemment interrompu.



=== Exercice 4: branching

Le fait de créer, fusionner et supprimer des *branches* est très facile avec git.

Inspirez vous de l'exercice 1 pour appliquer ce que vous avez appris aux branches.
Par exemple:

. Créez une branche.
  Modifiez un fichier source sur cette branche, ou créez-y un nouveau fichier.
  Commitez votre changement sur la branche.
  Repassez sur le `master` et fusionnez-y la branche.
  Supprimez la branche.

. La même chose en devant gérer un conflit.

Ensuite, utilisez *git flow* pour vous familiariser avec son workflow.
En vous mettant dans une des situations suivantes, entrez le commandes git flow correspondantes.
À chaque fois, essayez de bien comprendre les commandes git que votre commande git flow effectue à votre place !

. Développez une nouvelle fonctionnalité pour la prochaine version du logiciel.
. Préparez une nouvelle version du logiciel.
. Corrigez une anomalie sur la version actuelle.
  N'oubliez pas de vérifier que cette correction est bien propagée aussi pour vos collègues développeurs !
. Corrigez une anomalie signalée dans la version *précédente* du logiciel. +
  Indice: git flow ne suffira peut-être pas !



=== Exercice 5: patches

Les membres d'une équipe de développement n'ont pas toujours accès à un serveur git pour centraliser leurs travaux.
De même, il est possible qu'une partie de ces développeurs _ne souhaite pas_ passer par un serveur pour s'échanger leurs travaux. +
Dans ce cas, le support de communication le plus utilisé est le *patch*.
Un fichier de patch renferme toutes les modifications apportées à un dépôt afin qu'un autre dépôt puisse en profiter.

* Créez une branche dans votre premier dépôt.
  Effectuez quelques commits sur cette branche (peu importe leur contenu).

* Créez un fichier de patch avec la commande suivante:
+
....
git format-patch master --stdout > mes_modifs.patch
....

* Depuis votre second dépôt, observez les modifications apportées par le patch,
  mais _sans les appliquer_, grâche à la commande suivante:
+
....
git apply --stat /chemin/vers/mes_modifs.patch
....

* Depuis votre second dépôt, vérifiez si le patch s'applique correctement
  grâce à la commande suivante (s'il n'y a aucun output, c'est que c'est bon):
+
....
git apply --check /chemin/vers/mes_modifs.patch
....

* Depuis votre second dépôt, appliquez le patch grâce à la commande suivante:
+
....
git am --signoff /chemin/vers/mes_modifs.patch
....

* Analysez l'historique de votre second dépôt.
  Quel effet a eu l'option `--signoff` sur les commentaires de commit ?
  Pourquoi est-ce utile ?


=== Exercice 6: altération de l'historique

Étudiez les possibilités offertes par la commande `git rebase -i`.

Grâce à elle, il est possible d'éditer des commits existants, soit en altérant leur commentaire, soit directement leur contenu.

Un autre «besoin» rempli par cette commande est d'effectuer des link:https://www.ekino.com/articles/comment-squasher-efficacement-ses-commits-avec-git[*squash*,title=Petit guide d'utilisation de la commande git squash] de plusieurs commits.
En d'autre termes, cela consiste à fusionner plusieurs commits en un seul.
Quels avantages, et éventuellement quels inconvénients, voyez-vous à cela ?
Réfléchissez un peu, éventuellement avec vos collègues, puis débattez-en avec votre prof à ce sujet. 😉


=== Exercice 7: interfaces graphiques

Découvrez un exemple d'interface graphique pour git: *gitk*.
Pour cela, il suffit d'exécuter la commande `gitk` depuis l'intérieur de votre dépôt.

Vous aurez une vue plus complète de comment les concepts de git sont traduits graphiquement
si vous avez préalablement au moins fait les exercices 1 et 2.

gitk n'est évidemment qu'un _front-end_ graphique pour git link:https://git-scm.com/downloads/guis/[parmi d'autres,title=Liste de plusieurs interfaces graphiques pour git].
Si vous préférez fonctionner avec une interface graphique, prenez le temps de trouver la solution qui vous convient le plus.
Quelques critères de choix : fonctionnalités, ergonomie, plate-formes supportées, prix, performances, licence, ...

Gardez cependant en tête que, durant votre carrière, vous pourrez vous retrouver dans des situations où il vous sera impossible d'utiliser un outil graphique.
Par exemple, si vous devez dépanner en urgence un serveur de production, vous n'aurez parfois que la ligne de commande à votre disposition.


=== Exercice 8: serveur(s) git

Si votre environnement de développement le permet (réseau et éventuellement droits admin), mettez en place un serveur git.
Coordonnez vous avec un autre étudiant pour valider son bon fonctionnement.

La documentation se trouve link:https://git-scm.com/book/it/v2/Git-on-the-Server-Setting-Up-the-Server[par ici,title=Documentation sur la configuration d'un serveur git].

Même si, pour votre propre bénéfice, je vous recommande de configurer votre propre serveur git à l'occasion, dans le cadre de ce cours, une alternative rapide est d'utiliser des *services d'hébergement*.

==== Hébergement web

Il existe aujourd'hui de nombreux services d'hébergement tels que link:https://gitlab.com/[gitlab] ou link:https://github.com/[GitHub].
Hébergez votre dépôt git existant sur un de ces deux sites et explorez-en les fonctionnalités. +
En particulier, essayez de :

* lier vos commits à des _issues_ préalablement créées.
  Que sont les issues ?
  De quelle(s) manière(s) peuvent-elles aider au travail en groupe ?
* gérer des *pull request*.
  De quelle(s) manière(s) peuvent-elles aider au travail en groupe ?
* configurer un *serveur d'intégration continue* pour compiler votre code à chaque push.



== Pour aller plus loin

Les articles suivants vous permettront, à travers des cas d'utilisation concrets de git,
d'en apprendre davantage sur les problématiques, notamment sur les questions de sécurité,
qui peuvent survenir dans un cadre professionnel.

* Un https://sethrobertson.github.io/GitBestPractices/[guide de bonne pratiques,title=Bonnes pratiques liées à git] liées à git et à la gestion de version en général.
* Un link:https://mikegerwitz.com/papers/git-horror-story[article très intéressant,title=Expérience vécue de Mike Gerwitz, permettant de se convaince de la nécessité de signer ses commits] sur git et l'usurpation d'identité.
  On peut aussi trouver link:https://www.linux.com/tutorials/pgp-web-trust-core-concepts-behind-trusted-communication/[davantage de détails,title=Article de linux.com sur les clés PGP et la manière de les utiliser pour signer ses commits] sur la manière de bâtir un *réseau de confiance* grâce aux clés PGP.
* Comment link:https://help.github.com/articles/removing-sensitive-data-from-a-repository/[revenir en arrière,title=Article de l'aide de GitHub permettant d'altérer l'historique d'un dépôt existant] si par malheur vous avez commité des informations sensibles.
* Une manière très utilisée de link:https://semver.org/[numéroter vos versions logicielles] de manière compréhensible pour vos utilisateurs.
* Utiliser git, c'est pratique. Utiliser git avec link:https://github.com/Arkweid/lefthook[left hook,title=Dépôt GitHub de Left Hook], c'est encore plus pratique.
* link:https://gitmoji.carloscuesta.me/[gitmoji,title=Site web de gitmoji], une manière ludique permettant d'identifier rapidement ce que fait un _commit_ particulier.



== Cheat sheet

=== Initialisation

* Renseigner ses informations personnelles:
+
....
git config [--global] user.name "Obi-Wan Kenobi"
git config [--global] user.email "kenobi@jedicouncil.org"
....

* Activer la coloration de la sortie:
+
....
git config [--global] color.ui true
....

* Changer l'éditeur utilisé par git,
  notamment pour la rédaction des commentaires de commit,
  mais aussi le mode interactif:
+
....
git config [--global] core.editor EDITOR
....

* Initialiser un dépôt vide:
+
....
git init [PATH]
....

* Dupliquer un dépôt existant:
+
....
git clone REMOTE_PATH [DST_PATH]
....

=== Modification

* Ajouter un fichier à l'index:
+
....
git add [--patch] PATH
....

* Valider les changements apportés à l'index en une nouvelle version:
+
....
git commit [-m DESCRIPTION_MESSAGE] [PATH]
....

* Récupérer les changements d'un autre dépôt:
+
....
git pull [BRANCH]
....

* Retirer un fichier de l'index:
+
....
git reset HEAD [PATH]
....

* Annuler les modifications locales d'un fichier:
+
....
git checkout -- PATH
....

* Supprimer un fichier:
+
....
git rm PATH
....

=== Analyse

* Visualiser l'état d'un dépôt:
+
....
git status
....

* Visualiser les différences avec la (dernière) version:
+
....
git diff [VERSION]
....

* Visualiser les différences avec l'index:
+
....
git diff --staged
....

* Visualiser l'historique des versions:
+
....
git log [--pretty=oneline]
....

* Savoir qui a modifié quelle ligne d'un fichier, quand, et pourquoi :
+
....
git blame <fichier>
....

* Observer vers quel chemin pointe un dépôt:
+
....
git remote -v
....

* Ajuster son historique de plein de manières ... intéressantes (*squash*, *edit*, *reword*) :
+
....
git rebase -i [<hash>]
....
Cela peut être aussi contre-productif, voire dangereux !



=== git flow

Vous pouvez retrouver ces opérations link:https://danielkummer.github.io/git-flow-cheatsheet/index.fr_FR.html[sur cette page,title=Commandes utiles de git flow].

* Initialiser git flow:
+
....
git flow init
....

* Démarrer le développement d'une nouvelle fonctionnalité :
+
....
git flow feature start <description>
....
Cela crée une branche nommée `feature/<description>`, et vous bascule dessus.

* Terminer le développement d'une fonctionnalité :
+
....
git flow feature finish <description>
....
À ne pas faire sans avoir testé la branche !

* Démarrer la correction d'une anomalie en production :
+
....
git flow hotfix start <description>
....
Cela crée une branche nommée `hotfix/<description>`, et vous bascule dessus.

* Attester la correction d'une anomalie :
+
....
git flow hotfix finish <description>
....
Assurez-vous bien d'avoir effectivement corrigé l'anomalie, et de ne pas en avoir créé d'autre !

* Démarrer la validation d'une nouvelle version :
+
....
git flow release start <version>
....
Cela crée une branche nommée `release/<version>`, et vous bascule dessus.

* Terminer la validation d'une nouvelle version :
+
....
git flow release finish <version>
....
Assurez-vous bien d'avoir entièrement validé/recetté le logiciel !


=== Branches (sans git flow)

* Créer une branche:
+
....
git branch BRANCH_NAME
....

* Visualiser la branche courante:
+
....
git branch
....

* Changer la branche courante:
+
....
git checkout BRANCH_NAME
....

* Fusionner une branche dans la branche courante:
+
....
git merge BRANCH_NAME
....

* Supprimer une branche locale:
+
....
git branch -d BRANCH_NAME
....



== Références

* Documentation rapide de git: https://git.github.io/git-reference/
* Documentation complète de git: https://git-scm.com/documentation
* Cheat sheet de git: https://rogerdudler.github.io/git-guide/index.fr.html
* Créer automatiquement des `.gitignore`: https://gitignore.io (vérifiez que le résultat est approprié à votre projet!)
* Vous avez fait n'importe quoi sur votre dépôt ? http://ohshitgit.com

