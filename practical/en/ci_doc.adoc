﻿:source-highlighter: prettify
:source-highlighter: highlightjs

= TP : Documentation pipeline
Régis WITZ <rwitz@unistra.fr>
:doctype: book
:toc:
:toc-title:
:toclevels: 1



== Prerequisites

This exercices require you to link:https://www.ruby-lang.org/en/documentation/installation/[have ruby installed] on your computer. +
You have to install *asciidoctor*, too, using Exercise 0 as a guide.

Make sure you satisfy these prerequisites before starting these exercises.

== Exercise 0: Install asciidoctor

. If it is not already present, install asciidoctor via https://rubygems.org/pages/download[gem], ruby's package manager:
+
[source,bash]
----
ruby --version
gem install [--user] asciidoctor
asciidoctor --version
----

If you really can't install asciidoctor on your machine, you can still set up a continuous integration server.
To do this, download for example the https://raw.githubusercontent.com/asciidoctor/asciidoctor.org/master/docs/asciidoc-writers-guide.adoc[source] (in asciidoc format) of the https://asciidoctor.org/docs/asciidoc-writers-guide/[AsciiDoc Writer's Guide].
Then do the third exercise and, once your continuous integration is functional, do the first exercise.

[NOTE]
====
If you encounter a permission error with the `gem install` command, you have two choices:

* if you're admin on your machine, use `su` or `sudo`;
* otherwise, set the `GEM_HOME` variable to a path to a directory over which you have write rights,
  then try to install the gems again.
  For example:
+
[source,bash]
----
export GEM_HOME=~/mygems
gem install asciidoctor
$GEM_HOME/bin/asciidoctor mon_best_seller.adoc
----

====


== Exercise 1: Asciidoctor 101

Create a text file.
Using the reference as necessary, write a document of some kind. +
For example, write a fake user documentation, with several chapters including different sections.
Include images, tables, references to other chapters or external sites, code snippets, etc. in it. +
You can also try to reproduce the present topic of TP!

Name it anyway you want ; this practical work will assume you named it `my_best_seller.adoc`, in all modesty.

Monitor regularly your progress and your output by generating the corresponding web page:
[source,bash]
----
asciidoctor my_best_seller.adoc -o my_page.html
----



== Exercise 2: Moar backends!

As a true document pipeline generally produces different documents from a single input,
HTML is obviously not the only format that asciidoctor offers. +
For example, you can generate a PDF corresponding to your document by installing the corresponding backend:
[source,bash]
----
gem install asciidoctor-pdf --pre
asciidoctor-pdf my_best_seller.adoc -o my_printable_book.pdf
----

[NOTE]
====
If you only have an older version of Ruby available,
you may not be able to install `asciidoctor-pdf`.

In that case, you have to use some tricks requiring intermediate formats:
[source,bash]
----
asciidoctor my_best_seller.adoc -b docbook
xsltproc /usr/share/xml/docbook/stylesheet/docbook-xsl/fo/docbook.xsl my_schema.xml > my_markup.fo
fop -fo my_markup.fo -pdf my_printable_book.pdf
----
Even if you don't need to do this on your machine, it doesn't hurt to understand this and to know some additional formats.
====

Other output formats exist.
Try to understand the possibilities offered by `asciidoctor`, especially the `-b` (`--backend`) option.

If you are interested in LaTeX (_you should!_), I recommend you to study the `asciidoctor-latex` backend:
[source,bash]
----
gem install asciidoctor-latex --pre
asciidoctor-latex my_best_seller.adoc -o my_thesis.tex
----



== Exercise 3: Continuous integration

=== Exercise 3.1: Simple build

* Generate your user manual into an `index.html` file that is your project website.
* Generate your user manual into a PDF file and make it available via your website.

Do this using the link:https://gitlab.com[Gitlab platform] as long as link:https://docs.gitlab.com/ee/user/project/pages/[Gitlab pages].
All you have to do, in addition to _pusher_ your repository to Gitlab, is to create a `.gitlab-ci.yml` file at the root of your repository.

[source,title='.gitlab-ci.yml']
----
image: ruby

pages:
  script:
  - gem install compass zurb-foundation # CSS stylesheets generation
  - gem install prawn -v 2.1.0
  - gem install asciidoctor # HTML generation
# add here the generation of index.html and pdf files.
# create them in a subfolder named "public"

  artifacts:
    paths:
    - public

  only:
  - master
----



Don't hesitate to take a look at https://gitlab.com/uncoded/GL[your course repository] to see how this can be done !
Even if I do some stuff a tad differently than in this practical work.

=== Exercise 3.2: Less simple build

Now that you feel at ease with documentation generation and continuous integration, enrich your build with the follwing steps:

* Create a GitHub repository _with source code and unit tests in it_.
* Add to the root of your repository a `README.adoc` file that is the user documentation for your project.
* Each time you make changes to the code or user manual, your continuous integration Travis should build your project as follows :
** _compile_
** _if the code compiles, run unit tests_
** _if tests are successful, generate the documentation of your code_
** _if code documentation is generated:_
*** Generate your user manual.
    This should include a link to the latest correct version of your code documentation.
*** Generate your user manual in an `index.html` file which is your project's web site.
*** Generate your user manual in PDF format and make it available on your project website.

You can still use Gitlab for now, because there you only have to update your `.gitlab-ci.yml` file.

=== Exercise 3.3: Least simple build

Now, use Asciidoctor (documentation generator), GitHub (hosting) and https://travis-ci.org/[Travis CI] (continuous integration) to set up the same build process as in exercise 3.2.

To help you get started, here is a `.travis.yml` file, the configuration file for continuous integration with Travis CI.

[source,title='.travis.yml']
----
sudo: required

env:
# useful section for your environment variables
# /!\ DON'T WRITE ANY SENSIBLE INFO HERE!
# for this, use Travis CI features

before_install:
  - gem install prawn -v 2.1.0   # asciidoctor dependency
  - gem install asciidoctor      # to generate your HTML
  - gem install asciidoctor-pdf --pre # to generate your PDF

script:
# put here build and documentation generation

deploy:
  # github pages configuration
  provider: pages
  skip-cleanup: true
  github-token: $GITHUB_TOKEN
  keep-history: true
  committer-from-gh: true
  local-dir: gh-pages
  on:
    branch: master
----

If you want, you can take a look at link:https://github.com/wiztigers/GL[this old version of your course material] to help you find inspiration.
Be warned though, the PDF document is generated using something else than `asciidoctor-pdf`.



== Going further

Another advantage of setting up such a documentary pipeline is that the substance is differentiated from the style.
This separation appears not only in the fact of being able to generate several documents from the same text,
as we have seen, but also in being able to apply different style sheets, depending on the readership of each document.
The goal can also be compliance with the graphic charter of the company issuing the document.

Unfortunately, the style sheet is generally dependent on the output format required.
In practice, this means that, even if all the documents produced must follow the same graphic charter,
it will often be necessary to write as many different style sheets as there are target document formats.

Concerning the HTML backend, to give you an idea of what can be done with CSS (especially improved with https://sass-lang.com/[SASS] ^[https://fr.wikipedia.org/wiki/Sass_(language)[wiki]]^), you can start with https://github.com/asciidoctor/asciidoctor-stylesheet-factory[this github repository].


== References

* http://asciidoctor.org/docs/asciidoc-syntax-quick-reference[Quick reference]
* http://asciidoctor.org/docs/user-manual[User manual]
* _Lorem Ipsum_ (text blocks) generator : http://www.lipsum.com
* Project website creation with the GitHub pages: https://pages.github.com
* https://travis-ci.org/[Travis CI]
* https://sass-lang.com/[SASS] ^[https://en.wikipedia.org/wiki/Sass_(stylesheet_language)[wiki]]^
* A few reasons why I made you use Asciidoctor and not Markdown : link:https://www.adamhyde.net/whats-wrong-with-markdown/[What's wrong with Markdown ?]
